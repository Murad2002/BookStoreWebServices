
-- filling user_type table
INSERT INTO `USER_TYPE` (NAME) VALUES ('standard_user' );
INSERT INTO `USER_TYPE` (NAME) VALUES ('publisher' );
INSERT INTO `USER_TYPE` (NAME) VALUES ('author' );

-- fillin users table
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Murad','Qadirov','murad2002@gmail.com','Murad202','murad1234',1 );
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Elxan','Ismayilov','elxan111@bk.ru','Elxan11','elxan23',2 );
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Elxan','Elatli','elxan444@mail.ru','Elxan435','Elatli444',3 );
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Namiq','Huseynov','namiq.hsn@gmail.com','Namiq67','namiq123_',3 );
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Leyla','Qasimova','leyla.qasm32@gmail.com','leyla768','leyla45',2 );
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Movsum','Hesenli','mvsm333@bk.ru','Movsum67_','Movsum222',1 );
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Fidan','Xanlarova','fidan.xanlarova@mail.ru','Fidaan','fdn345',1 );
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Fuad','Hasanov','fuad666@gmail.com','Fuad666','fuad777',1 );
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Akif','Qadirov','akf444@gmail.com','Akif','Akif123',3 );
INSERT INTO  `USER` (NAME,SURNAME,EMAIL,USERNAME,PASSWORD,USER_TYPE_ID) VALUES ( 'Murad','Ibrahimov','ibrahim54@gmail.com','Ibrahim','ibrahim222',2 );

-- filling books table
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Great power of universe','Interesting information about universe',3,2);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Secret of Darkness','secret of darkness',4,5);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('History','Interesting information about history',9,10);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Biology','Interesting information about biology',3,5);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Math','Interesting information about math',4,2);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('English','Interesting information about english',9,10);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Chinese','Interesting information about chinese',9,2);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Math','Interesting information about geography',4,2);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Math','Interesting information about hospital',3,5);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Environment','Interesting information about environment',3,10);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Cars','Interesting information about cars',9,2);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Buildings','Interesting information about buildings',4,5);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Healthy Life','Interesting information about healthy life',4,10);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Sessions In java','Interesting information about java',9,2);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('C#','Interesting information about C#',9,5);
INSERT INTO `BOOK`  (NAME,DESCRIPTION,AUTHOR_ID,PUBLISHER_ID) VALUES ('Python','Interesting information about Python',3,10);

