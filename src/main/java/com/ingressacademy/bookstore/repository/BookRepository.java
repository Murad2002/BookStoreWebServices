package com.ingressacademy.bookstore.repository;

import com.ingressacademy.bookstore.entity.Book;
import com.ingressacademy.bookstore.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BookRepository  extends JpaRepository<Book,Long> {

    Page<Book> findAllByNameAndStatus(Pageable pageable, String name, Integer status);

    List<Book> findAllByStatus(Integer status);

    Book findByIdAndStatus(Long id, Integer status);

    List<Book> findAllByPublisherAndStatus(User publisher, Integer status);

}
