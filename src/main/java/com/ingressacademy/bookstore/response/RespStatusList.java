package com.ingressacademy.bookstore.response;

import lombok.Data;

@Data
public class RespStatusList {
    private RespStatus status;
}
