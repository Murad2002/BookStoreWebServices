package com.ingressacademy.bookstore.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespBook {

    private Long id;

    private String name;

    private String description;

    private RespUser publisher;

    private RespUser author;
}
