package com.ingressacademy.bookstore.controller;


import com.ingressacademy.bookstore.request.RequestBook;
import com.ingressacademy.bookstore.response.RespBook;
import com.ingressacademy.bookstore.response.RespStatusList;
import com.ingressacademy.bookstore.response.Response;
import com.ingressacademy.bookstore.service.BookService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
@Tag(name="book-controller")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping(value = "/bookList")
    public Response<List<RespBook>> getBookList() {
        return bookService.getBookList();
    }

    @GetMapping(value = "/Id/{id}")
    public Response<RespBook> getBookById(@PathVariable("id") Long id) {
        return bookService.getBookById(id);
    }

    @GetMapping("/name/{name}/{pageNumber}/{limit}")
    public Response<List<RespBook>> getBookByName(@PathVariable("name") String name,@PathVariable("pageNumber") Integer pageNumber,@PathVariable("limit") Integer limit) {
        return bookService.getBookByName(name,pageNumber,limit);
    }

    @GetMapping("/publisher")
    public Response<List<RespBook>> getBooksByUserNameAndSurName(@RequestParam String name, @RequestParam String surname) {
        return bookService.getBooksByPublisherNameAndSurname(name, surname);
    }

    @PostMapping(value = "/addBook")
    public RespStatusList addBook(@RequestBody RequestBook reqBook) {
        return bookService.addBook(reqBook);
    }



    @PutMapping(value = "/updateBookByPublisher")
    public RespStatusList updateBook(@RequestBody RequestBook reqbook) {
        return bookService.updateLesson(reqbook);

    }

}
