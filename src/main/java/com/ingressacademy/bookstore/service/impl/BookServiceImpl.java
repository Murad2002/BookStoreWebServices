package com.ingressacademy.bookstore.service.impl;

import com.ingressacademy.bookstore.entity.Book;
import com.ingressacademy.bookstore.entity.User;
import com.ingressacademy.bookstore.entity.UserType;
import com.ingressacademy.bookstore.enums.EnumAvailableStatus;
import com.ingressacademy.bookstore.exception.BookStoreException;
import com.ingressacademy.bookstore.exception.ExceptionConstants;
import com.ingressacademy.bookstore.repository.BookRepository;
import com.ingressacademy.bookstore.repository.UserRepository;
import com.ingressacademy.bookstore.repository.UserTypeRepository;
import com.ingressacademy.bookstore.request.RequestBook;
import com.ingressacademy.bookstore.response.*;
import com.ingressacademy.bookstore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserTypeRepository userTypeRepository;
    @Override
    public Response<List<RespBook>> getBookList() {
        Response<List<RespBook>> responseBook = new Response<>();
        try {
            List<RespBook> respBookList = bookRepository.findAllByStatus(EnumAvailableStatus.ACTIVE.getValue())
                    .stream()
                    .map(this::convertToRespBook)
                    .collect(Collectors.toList());
            if (respBookList.isEmpty()) {
                throw new BookStoreException(ExceptionConstants.BOOK_NOT_FOUND, "Book not found");
            }
            responseBook.setResponse(respBookList);
            responseBook.setStatus(RespStatus.getSuccessMessage());

        } catch (BookStoreException ex) {
            responseBook.setStatus(new RespStatus(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            responseBook.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return responseBook;
    }

    @Override
    public Response<RespBook> getBookById(Long id) {
        Response<RespBook> response = new Response<>();
        try {
            Book book = bookRepository.findByIdAndStatus(id,EnumAvailableStatus.ACTIVE.getValue());
            if (book == null) {
                throw new BookStoreException(ExceptionConstants.BOOK_NOT_FOUND, "Book not found");
            }
            RespBook respBook = new RespBook();
            respBook.setId(book.getId());
            respBook.setName(book.getName());
            respBook.setDescription(book.getDescription());
            RespUser author = new RespUser();
            author.setName(book.getAuthor().getName());
            author.setSurname(book.getAuthor().getSurname());
            respBook.setAuthor(author);
            RespUser publisher = new RespUser();
            publisher.setName(book.getPublisher().getName());
            publisher.setSurname(book.getPublisher().getSurname());
            respBook.setPublisher(publisher);

            response.setResponse(respBook);
            response.setStatus(RespStatus.getSuccessMessage());

        } catch (BookStoreException ex) {
            response.setStatus(new RespStatus(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response<List<RespBook>> getBookByName(String name, Integer pageNumber, Integer limit) {
        Response<List<RespBook>> response = new Response<>();
        List<RespBook> bookList = new ArrayList<>();
        try {
            Pageable pageRequest = PageRequest.of(pageNumber,limit);
            Page<Book>  books = bookRepository.findAllByNameAndStatus(pageRequest,name,EnumAvailableStatus.ACTIVE.getValue());
            if (books.isEmpty()) {
                throw new BookStoreException(ExceptionConstants.BOOK_NOT_FOUND, "Book not found");
            }
            for(Book book : books) {
                RespBook respBook = new RespBook();
                respBook.setId(book.getId());
                respBook.setName(book.getName());
                respBook.setDescription(book.getDescription());
                RespUser author = new RespUser();
                author.setName(book.getAuthor().getName());
                author.setSurname(book.getAuthor().getSurname());
                respBook.setAuthor(author);
                RespUser publisher = new RespUser();
                publisher.setName(book.getPublisher().getName());
                publisher.setSurname(book.getPublisher().getSurname());
                respBook.setPublisher(publisher);
                bookList.add(respBook);

            }
            response.setResponse(bookList);
            response.setStatus(RespStatus.getSuccessMessage());

        } catch (BookStoreException ex) {
            response.setStatus(new RespStatus(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response<List<RespBook>> getBooksByPublisherNameAndSurname(String name,String surName) {
        Response<List<RespBook>> response = new Response<>();
        try {
            User user = userRepository.findByNameAndSurnameAndStatus(name,surName, EnumAvailableStatus.ACTIVE.getValue());
            List<Book> bookList = bookRepository.findAllByPublisherAndStatus(user,EnumAvailableStatus.ACTIVE.getValue());
            if (bookList.isEmpty()) {
                throw new BookStoreException(ExceptionConstants.BOOK_NOT_FOUND, "Book not found");
            }
            List<RespBook> respBookList = new ArrayList<>();
            for (Book book : bookList) {
                RespBook respBook = new RespBook();
                respBook.setId(book.getId());
                respBook.setName(book.getName());
                respBook.setDescription(book.getDescription());
                RespUser author = new RespUser();
                author.setName(book.getAuthor().getName());
                author.setSurname(book.getAuthor().getSurname());
                respBook.setAuthor(author);
                RespUser publisher = new RespUser();
                publisher.setName(book.getPublisher().getName());
                publisher.setSurname(book.getPublisher().getSurname());
                respBook.setPublisher(publisher);
                respBookList.add(respBook);
            }
            response.setResponse(respBookList);
            response.setStatus(RespStatus.getSuccessMessage());

        } catch (BookStoreException ex) {
            response.setStatus(new RespStatus(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public RespStatusList addBook(RequestBook reqBook) {
        RespStatusList respStatusList = new RespStatusList();
        try {
            if(reqBook.getAuthorId() == null || reqBook.getPublisherId() == null || reqBook.getName()==null){
                throw new BookStoreException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid Request Data !");
            }
            List<UserType> userTypes = userTypeRepository.findAllByStatus(EnumAvailableStatus.ACTIVE.getValue());
            User publisher = userRepository.findByIdAndUserTypeAndStatus(reqBook.getPublisherId(),userTypes.get(1),EnumAvailableStatus.ACTIVE.getValue());
            User author = userRepository.findByIdAndUserTypeAndStatus(reqBook.getAuthorId(),userTypes.get(2),EnumAvailableStatus.ACTIVE.getValue());

            if(author==null || publisher==null){
                throw new BookStoreException(ExceptionConstants.PUBLISHER_OR_AUTHOR_NOT_FOUND,"Publisher or Author Not Found !");
            }
            Date date = new Date();
            Book book = new Book();
            book.setName(reqBook.getName());
            book.setDescription(reqBook.getDescription());
            book.setAuthor(author);
            book.setPublisher(publisher);
            book.setStatus(EnumAvailableStatus.ACTIVE.getValue());
            book.setInsertDate(date);
            respStatusList.setStatus(RespStatus.getSuccessMessage());
            bookRepository.save(book);
        }catch (BookStoreException ex) {
            respStatusList.setStatus(new RespStatus(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            respStatusList.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return respStatusList;

    }

    @Override
    public RespStatusList updateLesson(RequestBook reqBook) {
        RespStatusList respStatusList = new RespStatusList();
        try {
            if(reqBook.getPublisherId() == null || reqBook.getId() == null){
                throw new BookStoreException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid Request Data !");
            }
            Optional<UserType> userType = userTypeRepository.findById(new Long(2));
            User publisher = userRepository.findByIdAndUserTypeAndStatus(reqBook.getPublisherId(),userType.get(),EnumAvailableStatus.ACTIVE.getValue());
            if(publisher == null){
                throw new BookStoreException(ExceptionConstants.USER_NOT_FOUND,"Publisher Not Found !");
            }
            Book book = bookRepository.findByIdAndStatus(reqBook.getId(),EnumAvailableStatus.ACTIVE.getValue());
            if(book == null){
                throw new BookStoreException(ExceptionConstants.BOOK_NOT_FOUND,"Book not Found !");
            }
            List<Book> books = bookRepository.findAllByPublisherAndStatus(publisher,EnumAvailableStatus.ACTIVE.getValue());
            if(books.isEmpty()){
                throw new BookStoreException(ExceptionConstants.BOOK_NOT_FOUND,"No book found for this Publisher");
            }

            boolean result = false;
            for(Book b : books){
                if(b == book){
                    book.setName(reqBook.getName());
                    book.setDescription(reqBook.getDescription());
                    bookRepository.save(book);
                    result = true;
                    break;
                }
            }
            if(!result){
                throw new BookStoreException(ExceptionConstants.BOOK_NOT_FOUND,"This book does not Belong to this publisher !");
            }

            respStatusList.setStatus(RespStatus.getSuccessMessage());
            bookRepository.save(book);
        }catch (BookStoreException ex) {
            respStatusList.setStatus(new RespStatus(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            respStatusList.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return respStatusList;
    }

    public RespBook convertToRespBook(Book book){
        return RespBook.builder()
                .id(book.getId())
                .name(book.getName())
                .description(book.getDescription())
                .author(RespUser.builder().name(book.getAuthor().getName()).surname(book.getAuthor().getSurname()).build())
                .publisher(RespUser.builder().name(book.getPublisher().getName()).surname(book.getPublisher().getSurname()).build())
                .build();
    }
}
