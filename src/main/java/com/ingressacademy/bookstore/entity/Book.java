package com.ingressacademy.bookstore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "BOOK")
public class Book   {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne()
    @JoinColumn(name = "PUBLISHER_ID" ,nullable = false)
    private User publisher;

    @ManyToOne()
    @JoinColumn(name = "AUTHOR_ID" ,nullable = false)
    private User author;

    @Column(name = "STATUS")
    @ColumnDefault(value = "1")
    private Integer status;

    @Column(name = "INSERT_DATE")
    @ColumnDefault(value = "SYSDATE")
    private Date insertDate;

}
