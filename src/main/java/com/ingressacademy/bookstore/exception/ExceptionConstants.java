package com.ingressacademy.bookstore.exception;

public class ExceptionConstants {

    public static final Integer INTERNAL_EXCEPTION = 100;

    public static final Integer INVALID_REQUEST_DATA = 101;

    public static final Integer BOOK_NOT_FOUND = 102;

    public static final Integer USER_NOT_FOUND = 103;

    public static final Integer PUBLISHER_OR_AUTHOR_NOT_FOUND = 104;

    public static final Integer INVALID_EMAIL = 105;
}
